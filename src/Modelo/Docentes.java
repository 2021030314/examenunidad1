/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

public class Docentes {
    private String numDocente;
    private String nombre;
    private String domicilio;
    private String nivel;
    private float pagoB;
    private float horas;
    
    public Docentes(){
        
    }
    
    public Docentes(String numDocente, String nombre, String domicilio, String nivel, float pagoB, float horas){
        this.numDocente = numDocente;
        this.nombre = nombre;
        this.domicilio = domicilio;
        this.nivel = nivel;
        this.pagoB = pagoB;
        this.horas = horas;
    }
    
    public Docentes(Docentes DC){
        DC.numDocente = numDocente;
        DC.nombre = nombre;
        DC.domicilio = domicilio;
        DC.nivel = nivel;
        DC.pagoB = pagoB;
        DC.horas = horas;
    }

    public String getNumDocente() {
        return this.numDocente;
    }

    public String getNombre() {
        return this.nombre;
    }

    public String getDomicilio() {
        return this.domicilio;
    }

    public String getNivel() {
        return this.nivel;
    }

    public float getPagoB() {
        return this.pagoB;
    }

    public float getHoras() {
        return this.horas;
    }

    public void setNumDocente(String numDocente) {
        this.numDocente = numDocente;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public void setNivel(String nivel) {
        this.nivel = nivel;
    }

    public void setPagoB(float pagoB) {
        this.pagoB = pagoB;
    }

    public void setHoras(float horas) {
        this.horas = horas;
    }
    
    public float calcularPago(){
        if(this.nivel.equals("Licenciatura o Ingeniería")){
            this.nivel = "30.0";
        }
        if(this.nivel.equals("Maestro en ciencias")){
            this.nivel = "50.0";
        }
        if(this.nivel.equals("Doctor")){
            this.nivel = "100.0";
        }
        this.pagoB += this.pagoB * (Float.parseFloat(this.nivel) / 100);
        return this.pagoB * this.horas;
    }
    
    public float calcularImpuesto(){
        
        return this.pagoB * this.horas * (float) 0.16;
    }
    
    public float calcularBono(int cantidadHijos){
        if(cantidadHijos > 0 && cantidadHijos <= 2){
            return this.pagoB * (float) 0.5;
        }
        if(cantidadHijos > 2 && cantidadHijos <= 5){
            return this.pagoB * (float)0.10;
        }
        if(cantidadHijos > 5){
            return this.pagoB * (float)0.20;
        }
        else{
            return 0;
        }
    }
    
}
