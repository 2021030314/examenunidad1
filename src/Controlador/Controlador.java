/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;
import Modelo.Docentes;
import Vista.dlgDocentes;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

public class Controlador implements ActionListener {
    private Docentes DC;
    private dlgDocentes vista;

    private Controlador(Docentes DC, dlgDocentes vista){
        this.DC = DC;
        this.vista = vista;
        
        vista.boxNivel.addActionListener(this);
        vista.btnCancelar.addActionListener(this);
        vista.btnCerrar.addActionListener(this);
        vista.btnGuardar.addActionListener(this);
        vista.btnLimpiar.addActionListener(this);
        vista.btnMostrar.addActionListener(this);
        vista.btnNuevo.addActionListener(this);
        vista.spinnerHijos.addChangeListener(null);
    }
    
    private void iniciarV(){
        vista.setTitle("-¿- PAGO DOCENCIAS -?-");
        vista.setSize(670, 675);
        vista.setVisible(true);
    }
    
    
    public static void main(String[] args) {
        Docentes DC = new Docentes();
        dlgDocentes vista = new dlgDocentes(new JFrame(), true);
        Controlador CT = new Controlador(DC, vista);
        CT.iniciarV();
      
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == vista.btnCancelar){
            vista.btnLimpiar.doClick();
            vista.btnGuardar.setEnabled(false);
            vista.btnMostrar.setEnabled(false);
        }
        
        if(e.getSource() == vista.btnCerrar){
            int out = JOptionPane.showConfirmDialog(vista, "¿Desea salir del programa?", "CERRAR PROGRAMA",JOptionPane.OK_CANCEL_OPTION);
            if(out == JOptionPane.OK_OPTION){
                this.vista.setVisible(false);
                this.vista.dispose();
                System.exit(0); 
            }
        }
        
        if(e.getSource() == vista.btnNuevo){
            vista.btnGuardar.setEnabled(true);
            vista.txtNumDocente.setEnabled(true);
            vista.txtNombre.setEnabled(true);
            vista.txtDomicilio.setEnabled(true);
            vista.txtHoras.setEnabled(true);
            vista.txtPagoB.setEnabled(true);
            vista.boxNivel.setEnabled(true);
        }
        
        if(e.getSource() == vista.btnGuardar){
            int cont = 0;
            if(vista.txtNumDocente.getText().equals("") || vista.txtNombre.getText().equals("") || vista.txtDomicilio.getText().equals("")){
               JOptionPane.showMessageDialog(vista, "Por favor llene todos los campos");
            }
            else{
                DC.setNumDocente(vista.txtNumDocente.getText());
                DC.setNombre(vista.txtNombre.getText());
                DC.setDomicilio(vista.txtDomicilio.getText());
                cont += 1;
                
            }
            try{
                DC.setHoras(Float.parseFloat(vista.txtHoras.getText()));
                DC.setPagoB(Float.parseFloat(vista.txtPagoB.getText()));
                cont += 1;
            }
            catch(NumberFormatException ex){
                JOptionPane.showMessageDialog(vista, "Surgió el siguiente error: " + ex.getMessage());
            }
            DC.setNivel(vista.boxNivel.getSelectedItem().toString());
            vista.spinnerHijos.setEnabled(true);
        }
        
        if(e.getSource() == vista.btnLimpiar){
            vista.txtNumDocente.setText(null);
            vista.txtNombre.setText(null);
            vista.txtDomicilio.setText(null);
            vista.txtHoras.setText(null);
            vista.txtPagoB.setText(null);
            vista.boxNivel.setSelectedIndex(0);
            vista.spinnerHijos.setValue(0);
            vista.labelDescuentoImpuestos.setText(null);
            vista.labelPagoBono.setText(null);
            vista.labelPagoHoras.setText(null);
            vista.labelTotal.setText(null);
        }
        
        if(e.getSource() == vista.btnMostrar){
            int w;
            float x, y, z;
            
            vista.txtNumDocente.setText(DC.getDomicilio());
            vista.txtNombre.setText(DC.getNombre());
            vista.txtDomicilio.setText(DC.getDomicilio());
            vista.txtHoras.setText(String.valueOf(DC.getHoras()));
            vista.txtPagoB.setText(String.valueOf(DC.getPagoB()));
            vista.boxNivel.setSelectedItem(DC.getNivel());
            w = (int)vista.spinnerHijos.getValue();
            //vista.labelPagoBono.setText(String.valueOf(w));
            
            z = DC.calcularPago();
            y = DC.calcularImpuesto();
            x = DC.calcularBono(w);
            vista.labelPagoHoras.setText(String.valueOf(z));
            vista.labelDescuentoImpuestos.setText(String.valueOf(y));
            vista.labelPagoBono.setText(String.valueOf(x));
            vista.labelTotal.setText(String.valueOf(x + y + z));
        }
       
    }
}
